extern crate rand;
use rand::Rng;
use std::cmp::Ordering;
use std::io;

const NUM_TRIES: u32 = 7;
const NUM_GAMES: u32 = 5;

fn main() {
    let mut score: u32 = 0;
    for game_number in 0..NUM_GAMES {
        if game_number > 0 && !want_next_game() {
            break;
        }
        println!("playing game no {}", game_number);
        score += guess_loop(rand::thread_rng().gen_range(1, 101));
    }
    println!(
        "You've played well winning {} games of {}",
        score, NUM_GAMES
    );
}

fn guess_loop(secret_number: u32) -> u32 {
    println!("Guess the number");
    let mut won = false;
    for _ in 1..NUM_TRIES {
        let mut guess = String::new();
        println!("Please input the guess");
        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to get string");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(ex) => {
                println!("{}", ex);
                continue;
            }
        };

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small"),
            Ordering::Equal => {
                println!("You win");
                won = true;
                break;
            }
            Ordering::Greater => println!("Too big"),
        };
    }
    if !won {
        println!("Number of tries expired, you lose");
        0
    } else {
        1
    }
}

fn want_next_game() -> bool {
    println!("Wanna next game?");
    let mut prompt_answer = String::new();
    io::stdin()
        .read_line(&mut prompt_answer)
        .expect("No answer received");
    prompt_answer.trim() == "y" || prompt_answer.trim() == "yes"
}
